extern crate leveldb;
extern crate env_logger;

use leveldb::database::Database;
use leveldb::kv::KV;
use leveldb::options::{Options,WriteOptions,ReadOptions};
use std::path::Path;


fn main() {
    env_logger::init();

    let dbpath = Path::new("archive");
    let mut options = Options::new();
    options.create_if_missing = true;

    let mut dbconn = match Database::open(dbpath, options) {
        Ok(db) => { db },
        Err(e) => { panic!("failed to open database: {:?}", e) }
    };

    let write_opts = WriteOptions::new();
    match dbconn.put(write_opts, "foo", b"bar") {
        Ok(_) => { () },
        Err(e) => { panic!("failed to write to database: {:?}", e) }
    };

    println!("\n\nDone.");
}